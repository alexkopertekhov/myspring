package org.config.resources.configurators;

import atp.tp.ServiceConfigurator;
import atp.tp.annotations.ChainServiceConfigurators;
import org.injection.resources.services.SubService;

import java.lang.reflect.Field;

@ChainServiceConfigurators(order = 1, serviceName = "myServiceName")
public class ConfiguratorSserviceOne implements ServiceConfigurator {
    @Override
    public void configure(Object obj) throws ReflectiveOperationException {
        Field zero_config = obj.getClass().getField("zero_config");
        Field one_config = obj.getClass().getField("one_config");
        if (((int) zero_config.get(obj) == 34) && ((int) one_config.get(obj) == 17)) {
            one_config.setAccessible(true);
            one_config.set(obj, 71);
        }
    }
}
