package org.config.resources.configurators;

import atp.tp.ApplicationContext;
import atp.tp.ServiceConfigurator;
import atp.tp.annotations.ChainServiceConfigurators;
import atp.tp.annotations.Service;
import org.injection.resources.services.SubService;

import java.lang.reflect.Field;
import java.util.concurrent.ThreadLocalRandom;

@ChainServiceConfigurators(order = 0, serviceName = "myServiceName")
public class ConfiguratorSserviceZero implements ServiceConfigurator {

    private ApplicationContext applicationContext;

    public ConfiguratorSserviceZero(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }

    @Override
    public void configure(Object obj) throws ReflectiveOperationException {
        Field zero_config = obj.getClass().getField("zero_config");
        Field one_config = obj.getClass().getField("one_config");
        for (Field field : obj.getClass().getDeclaredFields()) {
            if (field.getType().equals(SubService.class)) {
                field.setAccessible(true);
                field.set(obj, applicationContext.getBean("mySubService"));
            }
        }
        if (((int) zero_config.get(obj) == 43) && ((int) one_config.get(obj) == 17)) {
            zero_config.setAccessible(true);
            zero_config.set(obj, 34);
        }
    }
}
