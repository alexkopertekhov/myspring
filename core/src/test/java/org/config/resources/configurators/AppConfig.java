package org.config.resources.configurators;

import atp.tp.annotations.Bean;
import atp.tp.annotations.Config;
import org.injection.resources.services.SubService;

@Config
public class AppConfig {
    @Bean(name = "mySubService")
    public SubService getSubService() {
        return new SubService().setId(15);
    }
}
