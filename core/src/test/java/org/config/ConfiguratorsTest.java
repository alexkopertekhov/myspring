package org.config;

import atp.tp.ApplicationContext;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.injection.resources.services.Sservice;
import org.injection.resources.services.SubService;

public class ConfiguratorsTest {
    Sservice service;
    ApplicationContext applicationContext;

    @Before
    public void runInject() throws ReflectiveOperationException {
        applicationContext = new ApplicationContext("org.config");
        service = applicationContext.createObject(Sservice.class);
    }

    @Test
    public void checkBean() throws ReflectiveOperationException {
        Assert.assertNotNull(applicationContext.getBean("mySubService"));
        Assert.assertEquals(((SubService) applicationContext.getBean("mySubService")).id, 15);
    }

    @Test
    public void checkConfiguratorZeroInjectMyBean() {
        Assert.assertEquals(service.getSubService().id, 15);
    }

    @Test
    public void checkOrderConfigurators() {
        Assert.assertEquals(service.zero_config, 34);
        Assert.assertEquals(service.one_config, 71);
    }
}
