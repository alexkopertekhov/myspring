package org.injection;

import atp.tp.ApplicationContext;
import org.injection.resources.services.LazyService;
import org.injection.resources.services.SubServiceForAutowired;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.injection.resources.services.Sservice;

public class ServiceTest {
    Sservice service;
    ApplicationContext applicationContext;

    @Before
    public void runInject() throws ReflectiveOperationException {
        applicationContext = new ApplicationContext("org.injection");
        service = applicationContext.createObject(Sservice.class);
    }

    @Test
    public void lazyTest() throws ReflectiveOperationException {
        Assert.assertEquals(LazyService.getCountLazyServices(), 0);
        applicationContext.getBean("LazyService");
        Assert.assertEquals(LazyService.getCountLazyServices(), 1);
    }

    @Test
    public void autowiredTest() throws ReflectiveOperationException {
        Assert.assertNotNull(service.getServiceForAutowired());
        Assert.assertEquals(service.getServiceForAutowired().getClass(), SubServiceForAutowired.class);
    }

    @Test
    public void createTest() {
        Assert.assertNotNull(service);
    }

    @Test
    public void injectionTest() {
        Assert.assertNotNull(service.getSubService());
        Assert.assertEquals(service.getSubService().id, 7);
    }

    @Test
    public void checkPostConstruct() {
        Assert.assertEquals(service.idPostConstruct, 1);
    }

    @Test
    public void cronTest() throws InterruptedException {
        if (service.countCronMethodInvoked < 2) {
            Thread.sleep(600);
        }
        Assert.assertEquals(service.countCronMethodInvoked, 2);
    }
}
