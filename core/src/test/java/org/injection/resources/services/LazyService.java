package org.injection.resources.services;

import atp.tp.annotations.Lazy;
import atp.tp.annotations.Service;

@Service
@Lazy
public class LazyService {

    private static int countLazyServices = 0;

    public static int getCountLazyServices() {
        return countLazyServices;
    }

    public LazyService() {
        countLazyServices++;
    }
}
