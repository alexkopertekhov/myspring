package org.injection.resources.services;

import atp.tp.annotations.Service;

@Service
public class SubService {
    public int id = 7;

    public SubService setId(int id) {
        this.id = id;
        return this;
    }
}
