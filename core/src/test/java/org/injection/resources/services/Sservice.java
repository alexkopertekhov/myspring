package org.injection.resources.services;

import atp.tp.annotations.Autowired;
import atp.tp.annotations.Cron;
import atp.tp.annotations.Service;

import javax.annotation.PostConstruct;

@Service(name = "myServiceName")
public class Sservice {
    public int countCronMethodInvoked = 0;

    public int idPostConstruct = 0;

    public int zero_config = 43;

    public int one_config = 17;

    public String proxyTestString = "string_without_proxy";

    private SubService subService;

    @Autowired
    private SubServiceForAutowired serviceForAutowired;

    public SubService getSubService() {
        return subService;
    }

    public SubServiceForAutowired getServiceForAutowired() {
        return serviceForAutowired;
    }

    @PostConstruct
    void changeIdPostConstruct() {
        idPostConstruct = 1;
    }

    @Cron(repeatMilliSec = 200)
    public void cronMethod() throws InterruptedException {
        if (countCronMethodInvoked == 2) {
            throw new InterruptedException();
        }
        countCronMethodInvoked++;
    }
}
