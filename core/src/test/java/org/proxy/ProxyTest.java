package org.proxy;

import atp.tp.ApplicationContext;
import org.injection.resources.services.Sservice;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ProxyTest {
    Sservice service;
    ApplicationContext applicationContext;

    @Before
    public void runInject() throws ReflectiveOperationException {
        applicationContext = new ApplicationContext("org.proxy");
        service = applicationContext.createObject(Sservice.class);
    }

    @Test
    public void checkProxyTestString() {
        Assert.assertTrue(service.proxyTestString.startsWith("Proxy"));
    }

    @Test
    public void checkOrderProxyConfigurators() {
        Assert.assertTrue(service.proxyTestString.endsWith("01"));
    }


}