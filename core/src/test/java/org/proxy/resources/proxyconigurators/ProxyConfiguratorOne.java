package org.proxy.resources.proxyconigurators;

import atp.tp.ServiceProxyConfigurator;
import atp.tp.annotations.ChainServiceProxyConfigurators;

import java.lang.reflect.Field;

@ChainServiceProxyConfigurators(order = 1, serviceName = "myServiceName")
public class ProxyConfiguratorOne implements ServiceProxyConfigurator {
    @Override
    public <T> T getWrappedProxyService(T service) throws ReflectiveOperationException {
        Field field = service.getClass().getField("proxyTestString");
        field.setAccessible(true);
        field.set(service, (((String)field.get(service)) + "1"));
        return service;
    }
}
