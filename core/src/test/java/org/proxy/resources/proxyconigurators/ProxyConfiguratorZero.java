package org.proxy.resources.proxyconigurators;

import atp.tp.ServiceProxyConfigurator;
import atp.tp.annotations.ChainServiceProxyConfigurators;

import java.lang.reflect.Field;

@ChainServiceProxyConfigurators(order = 0, serviceName = "myServiceName")
public class ProxyConfiguratorZero implements ServiceProxyConfigurator {
    @Override
    public <T> T getWrappedProxyService(T service) throws ReflectiveOperationException {
        Field field = service.getClass().getField("proxyTestString");
        field.setAccessible(true);
        field.set(service, ("Proxy" + ((String)field.get(service)) + "0"));
        return service;
    }
}
