package atp.tp.configurators;

import atp.tp.ApplicationContext;
import atp.tp.ServiceConfigurator;
import atp.tp.annotations.Service;
import java.lang.reflect.Field;

/**
 * The system class is a configurator
 * that inject dependencies for annotated "Service" classes
 */
public final class InjectServiceAnnotationApplicationContext implements ServiceConfigurator {
    private final ApplicationContext applicationContext;

    public InjectServiceAnnotationApplicationContext(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }

    @Override
    public void configure(Object obj) throws ReflectiveOperationException {
        for (Field field : obj.getClass().getDeclaredFields()) {
            if (field.getType().isAnnotationPresent(Service.class)) {
                field.setAccessible(true);
                field.set(obj, applicationContext.createObject(field.getType()));
            }
        }
    }
}
