package atp.tp.configurators;

import atp.tp.ApplicationContext;
import atp.tp.ServiceConfigurator;
import atp.tp.annotations.Autowired;
import java.lang.reflect.Field;

/**
 * The system class is a configurator
 * that inject dependencies for fields annotated with "Autowired".
 */
public class InjectAutowiredAnnotationApplicationContext implements ServiceConfigurator {
    private final ApplicationContext applicationContext;

    public InjectAutowiredAnnotationApplicationContext(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }

    @Override
    public void configure(Object obj) throws ReflectiveOperationException {
        for (Field field : obj.getClass().getDeclaredFields()) {
            if (field.isAnnotationPresent(Autowired.class)) {
                field.setAccessible(true);
                field.set(obj, applicationContext.createObject(field.getType()));
            }
        }
    }
}
