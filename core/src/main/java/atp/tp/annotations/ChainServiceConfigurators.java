package atp.tp.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Used to create a chain of configurator classes.
 * Marks the configurator class (of the class implementing the "ServiceConfigurator" interface).
 * It has the "order" parameter, which shows what the configurator class in this chain will be in order,
 * starting from 0, and the "serviceName" parameter indicating the service class to which this chain will be applied.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface ChainServiceConfigurators {
    int order() default 0;
    String serviceName() default "";
}
