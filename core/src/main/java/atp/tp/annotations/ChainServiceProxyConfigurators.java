package atp.tp.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Used to create a chain of proxy-configurator classes.
 * Marks the proxy-configurator class (of the class implementing the "ServiceProxyConfigurator" interface).
 * It has the "order" parameter, which shows what the proxy-configurator class in this chain will be in order,
 * starting from 0, and the "serviceName" parameter indicating the service class to which this chain will be applied.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface ChainServiceProxyConfigurators {
    int order() default 0;
    String serviceName() default "";
}
