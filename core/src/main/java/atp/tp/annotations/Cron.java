package atp.tp.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Used to create a cron method, you need to place this method in a service class,
 * where you need to pass the number of milliseconds with which you want this method to be repeated.
 * If you want this method to terminate, you need to throw the `InterruptedException` exception,
 * after which, the method call will stop (the program will not terminate with an exception)
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Cron {
    long repeatMilliSec();
}
