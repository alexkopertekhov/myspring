package atp.tp;

/**
 * Used to create proxy-configurator classes.
 * The proxy-configurator class needs to implement this interface and override the "getWrappedProxyService" method.
 */
public interface ServiceProxyConfigurator {

    /**
     *
     * @param service the service over which proxy configuration will take place
     * @return configured service
     * @throws ReflectiveOperationException
     */
    <T> T getWrappedProxyService(T service) throws ReflectiveOperationException;
}
