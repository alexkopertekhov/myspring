package atp.tp;

/**
 * Used to create configurator classes.
 * The configurator class needs to implement this interface and override the "configure" method.
 */
public interface ServiceConfigurator {

    /**
     *
     * @param obj the object on which the configuration will take place
     * @throws ReflectiveOperationException
     */
    void configure(Object obj) throws ReflectiveOperationException;
}
