package atp.tp;

import atp.tp.annotations.Cron;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * The system class that creates cron-methods.
 */
public class CronThread<T> extends Thread {
    private T service;
    private Method method;

    public CronThread<T> setService(T sservice) {
        this.service = sservice;
        return this;
    }

    public CronThread<T> setMethod(Method mmethod) {
        this.method = mmethod;
        return this;
    }

    @Override
    public void run() {
        while (true) {
            try {
                Thread.sleep(method.getAnnotation(Cron.class).repeatMilliSec());
                method.invoke(service);
            } catch (InterruptedException | InvocationTargetException | IllegalAccessException e) {
                break;
            }
        }
    }
}
