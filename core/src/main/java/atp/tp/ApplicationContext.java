package atp.tp;

import atp.tp.annotations.*;
import atp.tp.configurators.InjectAutowiredAnnotationApplicationContext;
import atp.tp.configurators.InjectServiceAnnotationApplicationContext;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.*;
import javax.annotation.PostConstruct;
import org.reflections.Reflections;

public final class ApplicationContext {
    private final Reflections source;
    private final List<ServiceConfigurator> configurators = new ArrayList<>();

    private final List<ServiceProxyConfigurator> proxyConfigurators = new ArrayList<>();

    private final Map<String, Object> beans = new HashMap<>();

    private final Map<Object, Method> cronMethods = new HashMap<>();

    private final List<Object> configClasses = new ArrayList<>();

    /**
     *
     * @param pathSource you need to pass the path to your project
     * @throws ReflectiveOperationException
     */
    public ApplicationContext(String pathSource) throws ReflectiveOperationException {

        Constructor<? extends ServiceConfigurator> constructorStandardConfiguratorService =
                InjectServiceAnnotationApplicationContext.class.getConstructor(ApplicationContext.class);
        configurators.add(constructorStandardConfiguratorService.newInstance(this));

        Constructor<? extends ServiceConfigurator> constructorStandardConfiguratorAutowired =
                InjectAutowiredAnnotationApplicationContext.class.getConstructor(ApplicationContext.class);
        configurators.add(constructorStandardConfiguratorAutowired.newInstance(this));

        source = new Reflections(pathSource);
        Set<Class<? extends ServiceConfigurator>> sourceConfigurators = source.getSubTypesOf(ServiceConfigurator.class);
        for (Class<? extends ServiceConfigurator> configurator : sourceConfigurators) {
            try {
                Constructor<? extends ServiceConfigurator> constructor =
                        configurator.getConstructor(ApplicationContext.class);
                configurators.add(constructor.newInstance(this));
            } catch (NoSuchMethodException e) {
                configurators.add(configurator.newInstance());
            } catch (ReflectiveOperationException e) {
                throw e;
            }
        }

        Set<Class<? extends ServiceProxyConfigurator>> sourceProxyConfigurators =
                source.getSubTypesOf(ServiceProxyConfigurator.class);
        for (Class<? extends ServiceProxyConfigurator> proxyConfigurator : sourceProxyConfigurators) {
            proxyConfigurators.add(proxyConfigurator.newInstance());
        }
        for (Class<?> configClass : source.getTypesAnnotatedWith(Config.class)) {
            configClasses.add(configClass.newInstance());
            for (Method method : configClass.getDeclaredMethods()) {
                if (method.isAnnotationPresent(Bean.class)) {
                    beans.put(method.getAnnotation(Bean.class).name(), method.invoke(
                            configClasses.get(configClasses.size() - 1)));
                }
            }
        }

        Set<Class<?>> lazyServices =
                source.getTypesAnnotatedWith(Lazy.class);
        for (Class<?> lazyService : lazyServices) {
            String name = lazyService.getAnnotation(Service.class).name().isEmpty() ? lazyService.getSimpleName()
                    : lazyService.getAnnotation(Service.class).name();
            beans.put(name, null);
        }
    }

    /**
     *
     * @param beanName the name of the service that you specified in the annotation @Service or @Bean,
     *                 if you did not specify it, then by default it is simpleName
     * @return service that was created in the process of interacting with ApplicationContext
     * @throws ReflectiveOperationException
     */

    public Object getBean(String beanName) throws ReflectiveOperationException {
        if (beans.get(beanName) == null) {
            for (Class<?> service : source.getTypesAnnotatedWith(Service.class)) {
                if (service.isAnnotationPresent(Lazy.class)) {
                    String name = service.getAnnotation(Service.class).name().isEmpty() ? service.getSimpleName()
                            : service.getAnnotation(Service.class).name();
                    if (beanName.equals(name)) {
                        return createObject(service);
                    }
                }
            }
        }
        return beans.get(beanName);
    }

    /**
     *
     * @param clazz the service you want to create
     * @return created service
     * @throws ReflectiveOperationException
     */
    public <T> T createObject(Class<T> clazz) throws ReflectiveOperationException {
        String serviceName = clazz.getAnnotation(Service.class).name().isEmpty() ? clazz.getSimpleName()
                : clazz.getAnnotation(Service.class).name();
        if (clazz.isAnnotationPresent(Lazy.class) && !beans.containsKey(serviceName)) {
            beans.put(serviceName, null);
            return null;
        }
        T instanceService = clazz.getDeclaredConstructor().newInstance();

        beans.put(serviceName, instanceService);

        configure(instanceService);

        beans.put(serviceName, instanceService);

        invokingPostConstructMethods(instanceService);

        beans.put(serviceName, instanceService);

        instanceService = wrappedProxyConfigurators(instanceService);

        beans.put(serviceName, instanceService);

        for (Method method : clazz.getDeclaredMethods()) {
            if (method.isAnnotationPresent(Cron.class)) {
                cronMethods.put(instanceService, method);
                new CronThread<T>().setMethod(method).setService(instanceService).start();
            }
        }

        return instanceService;
    }

    private <T> void configure(T service) throws ReflectiveOperationException {
        List<ServiceConfigurator> configuratorsForService = new ArrayList<>();
        for (ServiceConfigurator configurator : configurators) {
            if (service.getClass().isAnnotationPresent(Service.class)
                    && configurator.getClass().isAnnotationPresent(ChainServiceConfigurators.class)
                    && service.getClass().getAnnotation(Service.class).name().equals(configurator.getClass().
                    getAnnotation(ChainServiceConfigurators.class).serviceName())) {
                configuratorsForService.add(configurator);
            }
            if (!configurator.getClass().isAnnotationPresent(ChainServiceConfigurators.class)) {
                configurator.configure(service);
            }
        }
        configuratorsForService.sort(Comparator.
                comparingInt(x -> x.getClass().getAnnotation(ChainServiceConfigurators.class).order()));
        for (ServiceConfigurator configurator : configuratorsForService) {
            configurator.configure(service);
        }
    }

    private <T> void invokingPostConstructMethods(T service) throws ReflectiveOperationException {
        for (Method method : service.getClass().getDeclaredMethods()) {
            if (method.isAnnotationPresent(PostConstruct.class)) {
                method.setAccessible(true);
                method.invoke(service);
            }
        }
    }

    private <T> T wrappedProxyConfigurators(T service) throws ReflectiveOperationException {
        List<ServiceProxyConfigurator> proxyConfiguratorsForService = new ArrayList<>();
        for (ServiceProxyConfigurator proxyConfigurator : proxyConfigurators) {
            if (service.getClass().isAnnotationPresent(Service.class)
                    && proxyConfigurator.getClass().isAnnotationPresent(ChainServiceProxyConfigurators.class)
                    && service.getClass().getAnnotation(Service.class).name().equals(
                    proxyConfigurator.getClass().getAnnotation(ChainServiceProxyConfigurators.class).serviceName())) {
                proxyConfiguratorsForService.add(proxyConfigurator);
            }
            if (!proxyConfigurator.getClass().isAnnotationPresent(ChainServiceProxyConfigurators.class)) {
                service = proxyConfigurator.getWrappedProxyService(service);
            }
        }
        proxyConfiguratorsForService.sort(Comparator.
                comparingInt(x -> x.getClass().getAnnotation(ChainServiceProxyConfigurators.class).order()));
        for (ServiceProxyConfigurator proxyConfigurator : proxyConfiguratorsForService) {
            service = proxyConfigurator.getWrappedProxyService(service);
        }
        return service;
    }
}
