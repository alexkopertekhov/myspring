- Abstract factory - используется в проекте, для  инстанциация классов (ApplicationContext)
- Chain of responsibility - используется в проекте, для процесс настройки классов-сервисов (классы-(прокси)конфигураторы)
- Singleton - используется в проекте, для классы-сервисы(Lazy) и класс-конфигураторы
- Proxy - используется в проекте, для изменения поведения в рантайме