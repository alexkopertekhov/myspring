# MySpring

```
Author: Alexandr Kopertekhov
Version: 1.0.0
```

## Overview
> Данный фреймворк, реализует принципы IoC (Инверсии контроля) и DI (Внедрения зависимостей). Созданный API получился понятным для использования, а также гибким, чтобы его можно было применить в разработке произвольного проекта.

## Features
- Фреймворк предоставляет возможность автоматического инстанцирования классов-конфигураторов и классов-сервисов:
  - Класс-сервис: класс, заключающий в себе некую логику проекта.
  - Класс-конфигуратор: класс, настраивающий класс-сервис или внутреннюю логику работы вашего фреймворка.
- Поддерживается изменение поведения объекта (добавления новых возможностей, проверок при помощи аннотации)
- Настройка классов-сервисов может происходить в несколько стадий.
  - Присутсвует поддержка конфигураторов.
  - Присутствует поддержка прокси-конфигураторов.

## Core
- ### Создание класса-сервиса
  Для того, чтобы создать класс-сервис необходимо пометить данный класс аннотацией _@Service_, и присвоить параметру аннотации уникальное имя в дальнейшем данное имя будет использовано в классах-конфигураторов, для настройки.
```java
@Service(name = "serviceName")
public class Service {
...
}
```
- ### Внедрение (injection) сервисов происходит автоматически, методом (constructor inject), т.е. чтобы внедрить сервис, нужно объявить переменную в нашем классе, тип которой это класс, помеченный аннотацией _@Service_.
- ### Autowired также вы можете внедрить зависимость проаннотировав поле класса аннотацией _@Autowired_

```java
@Service(name = "serviceName")
public class Service {
  private SubServiceOne subServiceOne;
  private SubServiceTwo subServiceTwo;
  @Autowired
  private SubServiceThree subServiceThree;
    ...
}
```

- ### Создание Lazy Сервиса
  Для создания сервиса с ленивой инициализацией, его нужно пометить аннотацией @Lazy. Для его инициализации и дальнейшей работы с ним, получите его через метод _getBean(String serviceName)_, из вашего ApplicationContext (в дальнейшем будет более подробно рассказано что это).

```java
@Service(name = "lazyService")
@Lazy
public class LazyService {
    ...
}
```

- ### Создание классов-конфигураторов
  Для того, чтобы создать класс-конфигуратор необходимо реализовать в этом классе интерфейс _ServiceConfigurator_ и переопределить метод _configure_. Такой класс-конфигуратор будет общим для всех классов-сервисов. Если же вы хотите создать цепочку классов-конфигураторов, для конкретного класса, то необходимо пометить данный класс-конфигуратор аннотацией _@ChainServiceConfigurators(order = i, serviceName = "serviceName")_, где _order_ - это порядок в котором будут вызваны конфигураторы в даной цепочке, а _serviceName_ название класса-сервиса, для которого будет вызвана цепочка конфигураторов.
```java
@ChainServiceConfigurators(order = 0, serviceName = "serviceName")
public class ConfiguratorServiceZero implements ServiceConfigurator {
...
}
```
---
- ### Создание proxy-конфигураторов

  Аналогично, для классов-прокси-конфигураторов, реализуем ServiceProxyConfigurator, если необходимо создать цепочку помечаем аннотацией @ChainServiceProxyConfigurators(order = i, serviceName = "serviceName")

```java
@ChainServiceProxyConfigurators(order = 1, serviceName = "Radio")
public class ProxyConfigOne implements ServiceProxyConfigurator {
...
}
```
- ### @PostConstruct
  Если нам необходимо вызвать метод после работы внедрения зависимостей, то такой метод нужно пометить аннотацией @PostConstruct
```java
@PostConstruct
public void postConstructMethod() {
...
}
```

- ### @Cron
 Для создания cron-метода необходимо разместить данный метод в классе-сервисе и пометить данный метод аннотацией _Cron(long repeatMilliSec)_, куда нужно передать количество миллисекунд, с которым вы хотите, чтобы данный метод повторялся. Если вы хотите, чтобы данный метод завершился, необходимо выкинуть исключение `InterruptedException`, после чего, вызов метода прекратиться (программа, не завершиться с исключением)

```java
    @Cron(repeatMilliSec = 1000)
    public void cronMethod() throws InterruptedException {
        if(...) {
            throw new InterruptedException();
        }
        ...
    }
```

- ### Добавление нового класса-сервиса
  Если нам нужно сохранить свой бина, то необходимо создать конфигуровочный класс, пометить его аннотацией _@Config_ он является конфигуратором _ApplicationContext_ (_ApplicationContext_ - центральный класс данного фрейморка, который управляет конфигурированием и внедрением), создать метод, который будет возвращать нужный нам бин, и пометить данный метод аннотацией _@Bean(String name)_, где name - имя созданного нами бина.
  Чтобы обратиться к бинам, используйте метод ApplicationContext: _getBean(String beanName)_, по дефолту: _beanName = simpleClassName_. Пример использования данной возможности - подмена объекта при конфигурировании класса.

```java
@Config
public class ConfigApp {
    @Bean(name = "myService")
    public Service getMyService() {
        Service service = new Service();
        // setting service
        return service;
    }
}
```


- ### Начало работы
  Для начала работы фреймоворка требуется создать ApplicationContext(String source) в конструктор необходимо передать путь по которому будет содержаться ваши классы-сервисы и классы-конфигураторы. Далее необходимо вызвать метод _createObject(Class clazz)_, у созданного ApplicationContext, в который передать главный класс-сервис (в котором находиться бизнес логика). Метод вернёт созданный объект этого класса со всеми сконфигурированными и внедрёнными зависимостями.

```java
public static void main(String[] args) throws ReflectiveOperationException {
        MainService mainService = new ApplicationContext("path_to_your_classes")
                                                .createObject(MainService.class);
        MainService.start();
}
```

## General Sequence of Actions
<img src="docs/sequence_of_action.png">

## Example
> Пример того, как это пользоваться данным фреймворком, находиться в пакете example
Наш пример, радио - это наш главный сервис, ведущий это сервис, который надо проинджектить в класс радио, мы помечаем оба класса аннотацией @Service
и объявляем в приватном поле ведущего (private ShowMan showMan), тем самым инджектим его в наш главный сервис - радио, фреймворк его создаст, если у ведущего будут свои сервисы фрейморк также их сконфигурирует и проинджектить. В классе ConfigApp мы добавляем новый бин - своего ведущего. Далее в цепочке классов конфигураторов inject-им его в сервис радио. После вызываем postConstructMethod.
Далее в цепочке ProxyConfigurator-ов подменяем не угодную нам строчку.
 
 ## Diagram classes
<img src="docs/MySpringClassDiagramPng.png">

## Requirements
- java 1.8
- Apach Maven 3.9.0

## Building and running

mySpring  
|--core  
|&nbsp; &nbsp; &nbsp; \`-- src  
|&nbsp; &nbsp; &nbsp; &nbsp; -- target  
|&nbsp; &nbsp; &nbsp; &nbsp; -- pom.xml  
|--example  
|&nbsp; &nbsp; &nbsp; \`-- src  
|&nbsp; &nbsp; &nbsp; &nbsp; -- target  
|&nbsp; &nbsp; &nbsp; &nbsp; -- pom.xml  
|--exampleJar  
|&nbsp; &nbsp; &nbsp; \`-- libs  
|&nbsp; &nbsp; &nbsp; &nbsp; -- mySpring-1.0.0.jar  

Находясь в директории `core` выполните : `mvn clean install`  
При этом в target создастся jar-файл _mySpring-1.0.0.jar_.  

Далее для сборки примера перейдите в директорию `example` и выполните: `mvn clean install`  
При этом создастся директория `exampleJar`, в ней будет директория `libs`, в которой будут находиться dependency, необходимые для примера и jar-файл _example-1.0-SNAPSHOT.jar_  

Для запуска примера находясь в директории `exampleJar`,  
выполните: `java -jar example-1.0-SNAPSHOT.jar`  
