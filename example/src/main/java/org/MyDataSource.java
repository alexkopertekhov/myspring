package org;

import atp.tp.annotations.Lazy;
import atp.tp.annotations.Service;
import javax.annotation.PostConstruct;

@Service(name = "data")
@Lazy
public class MyDataSource {
    private String weather = "sunny";

    public String getWeather() {
        return weather;
    }

    @PostConstruct
    public void postConstructMethod() {
        System.out.println("DataSourceInstanced");
    }
}
