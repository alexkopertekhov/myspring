package org;

import atp.tp.annotations.Bean;
import atp.tp.annotations.Config;

@Config
public final class ConfigApp {

    @Bean(name = "myShowMan")
    public ShowMan getShowMan() {
        return new ShowMan().setName("BeanName");
    }
}
