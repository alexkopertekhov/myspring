package org;

import atp.tp.ApplicationContext;

public final class Main {
    private Main() {
    }

    public static void main(String[] args) throws ReflectiveOperationException {
        ApplicationContext applicationContext = new ApplicationContext("org");
        Radio radio = applicationContext.createObject(Radio.class);
        radio.show();
        radio.marketing();
        System.out.println("weather: " + ((MyDataSource) applicationContext.getBean("data")).getWeather());
        System.out.println("Hello world!");
    }
}
