package org;

import atp.tp.annotations.Service;

@Service(name = "ShowMan")
public final class ShowMan {
    private String name = "DefaultName";

    public String getName() {
        return name;
    }

    ShowMan setName(String nameShowMan) {
        this.name = nameShowMan;
        return this;
    }

    public void show() {
        System.out.println("Hi! I think you all know...");
    }

    public void say(final String speech) {
        System.out.println(speech);
    }
}
