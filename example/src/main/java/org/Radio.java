package org;

import atp.tp.annotations.Cron;
import atp.tp.annotations.Service;
import javax.annotation.PostConstruct;

@Service(name = "Radio")
public final class Radio {

    private int countRepeatCronMethod;
    private String marketingMessage = "HSE the best !!!";

    private ShowMan showMan;

    public void show() {
        showMan.show();
    }

    public void marketing() {
        showMan.say(marketingMessage);
    }

    @Cron(repeatMilliSec = 1000)
    public void cron() throws InterruptedException {
        if (countRepeatCronMethod == 3) {
            throw new InterruptedException();
        }
        System.out.println("Hello everyone, connect to 105.7 FM!!!");
        countRepeatCronMethod++;
    }

    @PostConstruct
    public void postConstructMethod() {
        System.out.println("postConstructMethodWorked");
    }
}
