package org.proxyConfigurators;

import atp.tp.ServiceProxyConfigurator;
import atp.tp.annotations.ChainServiceProxyConfigurators;
import java.lang.reflect.Field;
import org.Radio;

@ChainServiceProxyConfigurators(order = 0, serviceName = "Radio")
public final class ProxyConfig0 implements ServiceProxyConfigurator {

    @Override
    public <T> T getWrappedProxyService(T service) throws ReflectiveOperationException {
        System.out.println("wrap_start");
        if (service.getClass().equals(Radio.class)) {
            for (Field field : service.getClass().getDeclaredFields()) {
                if (field.getName().equals("marketingMessage")) {
                    field.setAccessible(true);
                    System.out.println("marketingMessage: " + field.get(service));
                }
            }
        }
        return service;
    }
}
