package org.proxyConfigurators;

import atp.tp.ServiceProxyConfigurator;
import atp.tp.annotations.ChainServiceProxyConfigurators;
import java.lang.reflect.Field;
import org.Radio;

@ChainServiceProxyConfigurators(order = 2, serviceName = "Radio")
public final class ProxyConfig2 implements ServiceProxyConfigurator {

    @Override
    public <T> T getWrappedProxyService(T service) throws ReflectiveOperationException {
        if (service.getClass().equals(Radio.class)) {
            for (Field field : service.getClass().getDeclaredFields()) {
                if (field.getName().equals("marketingMessage")) {
                    field.setAccessible(true);
                    System.out.println("marketingMessage: " + field.get(service));
                }
            }
        }
        System.out.println("wrap_end");
        return service;
    }
}
