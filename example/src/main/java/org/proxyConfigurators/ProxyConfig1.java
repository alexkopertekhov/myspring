package org.proxyConfigurators;

import atp.tp.ServiceProxyConfigurator;
import atp.tp.annotations.ChainServiceProxyConfigurators;
import java.lang.reflect.Field;
import org.Radio;

@ChainServiceProxyConfigurators(order = 1, serviceName = "Radio")
public final class ProxyConfig1 implements ServiceProxyConfigurator {

    @Override
    public <T> T getWrappedProxyService(T service) throws ReflectiveOperationException {
        if (service.getClass().equals(Radio.class)) {
            for (Field field : service.getClass().getDeclaredFields()) {
                if (field.getName().equals("marketingMessage")) {
                    field.setAccessible(true);
                    field.set(service, "MIPT the best !!!");
                }
            }
        }
        System.out.println("marketingMessageWrapProxy");
        return service;
    }
}
