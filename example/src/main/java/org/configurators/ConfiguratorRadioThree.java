package org.configurators;

import atp.tp.ServiceConfigurator;
import atp.tp.annotations.ChainServiceConfigurators;
import java.lang.reflect.Field;
import org.Radio;
import org.ShowMan;

@ChainServiceConfigurators(order = 2, serviceName = "Radio")
public class ConfiguratorRadioThree implements ServiceConfigurator {

    @Override
    public void configure(Object obj) throws ReflectiveOperationException {
        if (obj.getClass().equals(Radio.class)) {
            for (Field field : obj.getClass().getDeclaredFields()) {
                if (field.getType().equals(ShowMan.class)) {
                    field.setAccessible(true);
                    ShowMan showMan = (ShowMan) field.get(obj);
                    System.out.println("Ref: " + field.get(obj).toString() + "; ShowMan name: " + showMan.getName());
                }
            }
        }
        System.out.println("config_end");
    }
}
