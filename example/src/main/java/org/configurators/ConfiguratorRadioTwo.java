package org.configurators;

import atp.tp.ApplicationContext;
import atp.tp.ServiceConfigurator;
import atp.tp.annotations.ChainServiceConfigurators;
import java.lang.reflect.Field;
import org.Radio;
import org.ShowMan;

@ChainServiceConfigurators(order = 1, serviceName = "Radio")
public class ConfiguratorRadioTwo implements ServiceConfigurator {
    private final ApplicationContext applicationContext;

    public ConfiguratorRadioTwo(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }

    @Override
    public void configure(Object obj) throws ReflectiveOperationException {
        Radio radio = (Radio) applicationContext.getBean("Radio");
        for (Field field : radio.getClass().getDeclaredFields()) {
            if (field.getType().equals(ShowMan.class)) {
                field.setAccessible(true);
                field.set(radio, applicationContext.getBean("myShowMan"));
            }
        }
        System.out.println("configShowMan");
    }
}
